package co.gov.sgc.powercontrol;

import co.gov.sgc.powercontrol.verticles.TCPClientVerticle;
import co.gov.sgc.powercontrol.verticles.DBClientVerticle;
import co.gov.sgc.powercontrol.verticles.HeartbeatVerticle;
import co.gov.sgc.powercontrol.verticles.InterrupterVerticle;
import co.gov.sgc.powercontrol.verticles.LoggerVerticle;
import co.gov.sgc.powercontrol.verticles.Server;
import io.vertx.core.Vertx;

/**
 *
 * @author Julian Peña
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        //Vertx init
        Vertx vertx = Vertx.vertx();

        //Deployment chain
        vertx.deployVerticle(new LoggerVerticle(), loggerDeployment -> {
            if (loggerDeployment.failed()) {
                System.out.println("Deployment error!");
                System.exit(1);
            } else {
                vertx.eventBus().publish(LoggerVerticle.ADDRESS, "Logger deployed");
//                vertx.deployVerticle(new DBClientVerticle(), dbClientDeployment -> {
//                    if (dbClientDeployment.failed()) {
//                        vertx.eventBus().publish(LoggerVerticle.ADDRESS, "Deployment problem: DBClient");
//                        System.exit(1);
//                    } else {
                        vertx.eventBus().publish(LoggerVerticle.ADDRESS, "DBClient deployed");
                        vertx.deployVerticle(new TCPClientVerticle(), commandSenderDeployment -> {
                            if (commandSenderDeployment.failed()) {
                                vertx.eventBus().publish(LoggerVerticle.ADDRESS, "Deployment problem: CommandSender");
                                System.exit(1);
                            } else {
                                vertx.eventBus().publish(LoggerVerticle.ADDRESS, "CommandSender deployed");
                                vertx.deployVerticle(new HeartbeatVerticle(), hearbeatDeployment -> {
                                    if (hearbeatDeployment.failed()) {
                                        vertx.eventBus().publish(LoggerVerticle.ADDRESS, "Deployment problem: Hearbeat");
                                        System.exit(1);
                                    } else {
                                        vertx.eventBus().publish(LoggerVerticle.ADDRESS, "Hearbeat deployed");
                                        vertx.deployVerticle(new InterrupterVerticle(), interrupterDeployment -> {
                                            if (interrupterDeployment.failed()) {
                                                vertx.eventBus().publish(LoggerVerticle.ADDRESS, "Deployment problem: Interrupter");
                                                System.exit(1);
                                            } else {
                                                vertx.eventBus().publish(LoggerVerticle.ADDRESS, "Interrupter deployed");
                                                vertx.deployVerticle(new Server(), serverDeployment -> {
                                                    if (serverDeployment.failed()) {
                                                        vertx.eventBus().publish(LoggerVerticle.ADDRESS, "Deployment problem: Server");
                                                        System.exit(1);
                                                    }else{
                                                        vertx.eventBus().publish(LoggerVerticle.ADDRESS, "Server deployed");
                                                    }
                                                });
                                            }
                                        });
                                    }
                                });
                            }
                        });
//                    }
//                });
            }
        });

    }

}
