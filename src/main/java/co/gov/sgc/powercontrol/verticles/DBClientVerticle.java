package co.gov.sgc.powercontrol.verticles;

import co.gov.sgc.powercontrol.pojos.Relay;
import co.gov.sgc.powercontrol.pojos.Station;
import com.google.gson.Gson;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.jdbc.JDBCClient;
import io.vertx.ext.sql.SQLConnection;
import java.util.ArrayList;

/**
 *
 * @author Julian Peña
 */
public class DBClientVerticle extends AbstractVerticle {

    public static final String QUERY_ADDRESS = "DBClientVerticleGetAddress";
    public static final String PUT_ADDRESS = "DBClientVerticleSetAddress";

    public static final String QUERY_ALL_STATIONS = "getStations";
    public static final String QUERY_STATION_RELAYS = "getRelays";

    public static final String QUERY_ADD_STATION = "addStation";
    public static final String QUERY_EDIT_STATION = "editStation";
    public static final String QUERY_DEL_STATION = "delStation";

    public static final String QUERY_ADD_RELAY = "addRelay";
    public static final String QUERY_EDIT_RELAY = "editRelay";
    public static final String QUERY_DEL_RELAY = "delRelay";

    private JDBCClient jdbcClient;

    private Gson gson;

    @Override
    public void start() throws Exception {

        super.start();

        HikariConfig hikariConfig = new HikariConfig("database.properties");
        HikariDataSource datasource = new HikariDataSource(hikariConfig);

        jdbcClient = JDBCClient.create(vertx, datasource);

        gson = new Gson();

        vertx.eventBus().consumer(QUERY_ADDRESS, addressHandler -> {

            String[] message = ((String) addressHandler.body()).split(",");
            String query = message[0];

            String parameter = "";
            if (message.length > 1) {
                parameter = message[1];
            }

            switch (query) {
                case QUERY_ALL_STATIONS: {
                    getStations(addressHandler.replyAddress());
                }
                break;

                case QUERY_STATION_RELAYS: {
                    getRelays(Long.parseLong(parameter), addressHandler.replyAddress());
                }
                break;

                default:
                    vertx.eventBus().send(LoggerVerticle.ADDRESS, "Invalid query: " + query);
                    break;
            }

        });
        //consumer crud ADD station
        vertx.eventBus().consumer(QUERY_ADD_STATION, handler -> {
            Station station = Json.decodeValue(handler.body().toString(), Station.class);
            addStation(handler.replyAddress(), station);
        });
        //consumer crud EDIT station
        vertx.eventBus().consumer(QUERY_EDIT_STATION, handler -> {
            Station station = Json.decodeValue(handler.body().toString(), Station.class);
            editStation(handler.replyAddress(), station);
        });
        //consumer crud DEL station
        vertx.eventBus().consumer(QUERY_DEL_STATION, handler -> {
            int id = Json.decodeValue(handler.body().toString(), Integer.class);
            delStation(handler.replyAddress(), id);
        });
        //consumer crud ADD relay
        vertx.eventBus().consumer(QUERY_ADD_RELAY, handler -> {
            Relay relay = Json.decodeValue(handler.body().toString(), Relay.class);
            System.out.println(relay.getName());
            //addRelay(handler.replyAddress(), relay);
        });
        //consumer crud EDIT relay
        vertx.eventBus().consumer(QUERY_EDIT_STATION, handler -> {
            Relay relay = Json.decodeValue(handler.body().toString(), Relay.class);
            editRealy(handler.replyAddress(),relay);
        });
        //consumer crud DEL ralay
        vertx.eventBus().consumer(QUERY_DEL_STATION, handler -> {
            int id = Json.decodeValue(handler.body().toString(), Integer.class);
            delRelay(handler.replyAddress(), id);
        });
    }

    /**
     * Return all the stations on database.
     *
     * @param replyAddress The vertx event bus address on which the response
     * should be sent
     */
    private void getStations(String replyAddress) {

        jdbcClient.getConnection(connectionHandler -> {

            if (connectionHandler.failed()) {
                vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't get a connection");
                vertx.eventBus().send(replyAddress, "Error, please check the log");
            } else {

                SQLConnection connection = connectionHandler.result();
                connection.query("select id,name,ip,port from stations", queryHandler -> {

                    if (queryHandler.failed()) {
                        vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't query for stations");
                    } else {

                        ArrayList<Station> stations = new ArrayList<>();

                        for (JsonArray line : queryHandler.result().getResults()) {
                            Station station = new Station(line.getDouble(0), line.getString(1), line.getString(2), line.getInteger(3));
                            stations.add(station);
                        }

                        vertx.eventBus().send(replyAddress, gson.toJson(stations));
                    }

                });
            }
        });
    }

    /**
     * Add station on the BD
     *
     * @param replyAddress
     * @param station
     */
    private void addStation(String replyAddress, Station station) {
        jdbcClient.getConnection(connectionHandler -> {

            if (connectionHandler.failed()) {
                vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't get a connection");
                vertx.eventBus().send(replyAddress, "Error, please check the log");
            } else {
                SQLConnection connection = connectionHandler.result();
                connection.execute("INSERT INTO stations ( name, ip, port ) "
                        + "VALUES ('" + station.getName() + "','" + station.getIp() + "'," + station.getPort() + ");", queryHandler -> {
                            if (queryHandler.failed()) {
                                vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't query for stations add");
                            } else {
                                vertx.eventBus().send(LoggerVerticle.ADDRESS, "Ok query for stations add");
                            }

                        });
            }
        });

    }

    /**
     * Edit station
     *
     * @param replyAddress
     * @param station
     */
    private void editStation(String replyAddress, Station station) {
        jdbcClient.getConnection(connectionHandler -> {

            if (connectionHandler.failed()) {
                vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't get a connection");
                vertx.eventBus().send(replyAddress, "Error, please check the log");
            } else {

                SQLConnection connection = connectionHandler.result();
                connection.execute("UPDATE stations  SET name = '" + station.getName() + "', ip='" + station.getIp() + "', port=" + station.getPort() + " where id=" + station.getId() + "", queryHandler -> {
                    if (queryHandler.failed()) {
                        vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't query for stations edit");
                    } else {
                        vertx.eventBus().send(LoggerVerticle.ADDRESS, "Ok query for stations edit");
                    }

                });
            }
        });
    }

    /**
     * Delete station
     *
     * @param replyAddress
     * @param id
     */
    private void delStation(String replyAddress, int id) {
        jdbcClient.getConnection(connectionHandler -> {

            if (connectionHandler.failed()) {
                vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't get a connection");
                vertx.eventBus().send(replyAddress, "Error, please check the log");
            } else {
                SQLConnection connection = connectionHandler.result();
                connection.execute("delete from stations where id = " + id + "", queryHandler -> {
                    if (queryHandler.failed()) {
                        vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't query for stations edit");
                    } else {
                        vertx.eventBus().send(LoggerVerticle.ADDRESS, "Ok query for stations edit");
                    }

                });
            }
        });
    }

    /**
     * Return all the relays for the given station.
     *
     * @param replyAddress The vertx event bus address on which the response
     * should be sent
     */
    private void getRelays(double stationID, String replyAddress) {

        jdbcClient.getConnection(connectionHandler -> {

            if (connectionHandler.failed()) {
                vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't get a connection");
                vertx.eventBus().send(replyAddress, "Error, please check the log");
            } else {

                SQLConnection connection = connectionHandler.result();
                connection.query("select id,name,code,status from relays where station_id = " + stationID + ";", queryHandler -> {

                    if (queryHandler.failed()) {
                        vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't query for relays");
                        return;
                    }

                    ArrayList<Relay> relays = new ArrayList<>();

                    for (JsonArray line : queryHandler.result().getResults()) {
                        Relay relay = new Relay(line.getDouble(0), line.getString(1), line.getString(2), line.getBoolean(3), stationID);
                        relays.add(relay);
                    }

                    vertx.eventBus().send(replyAddress, gson.toJson(relays));
                });
            }
        });
    }

    private void addRelay(String replyAddress, Relay relay) {
        jdbcClient.getConnection(connectionHandler -> {

            if (connectionHandler.failed()) {
                vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't get a connection");
                vertx.eventBus().send(replyAddress, "Error, please check the log");
            } else {
                SQLConnection connection = connectionHandler.result();
                connection.execute("INSERT INTO relays ( code, name, status, statios_id ) "
                        + "VALUES ('" + relay.getCode() + "','" + relay.getName() + "'," + relay.getStatus() + "." + relay.getStationId() + ");", queryHandler -> {
                            if (queryHandler.failed()) {
                                vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't query for stations add");
                            } else {
                                vertx.eventBus().send(LoggerVerticle.ADDRESS, "Ok query for stations add");
                            }

                        });
            }
        });

    }

    private void editRealy(String replyAddress, Relay relay) {
        jdbcClient.getConnection(connectionHandler -> {

            if (connectionHandler.failed()) {
                vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't get a connection");
                vertx.eventBus().send(replyAddress, "Error, please check the log");
            } else {

                SQLConnection connection = connectionHandler.result();
                connection.execute("UPDATE stations  SET name = '" + relay + "', ip='" + relay + "', port=" + relay + " where id=" + relay.getId() + "", queryHandler -> {
                    if (queryHandler.failed()) {
                        vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't query for stations edit");
                    } else {
                        vertx.eventBus().send(LoggerVerticle.ADDRESS, "Ok query for stations edit");
                    }

                });
            }
        });

    }

    private void delRelay(String replyAddress, int idRelay) {
        jdbcClient.getConnection(connectionHandler -> {

            if (connectionHandler.failed()) {
                vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't get a connection");
                vertx.eventBus().send(replyAddress, "Error, please check the log");
            } else {
                SQLConnection connection = connectionHandler.result();
                connection.execute("delete from realys where id = " + idRelay + "", queryHandler -> {
                    if (queryHandler.failed()) {
                        vertx.eventBus().send(LoggerVerticle.ADDRESS, "Database problem. Couldn't query for stations edit");
                    } else {
                        vertx.eventBus().send(LoggerVerticle.ADDRESS, "Ok query for stations edit");
                    }

                });
            }
        });

    }

}
