package co.gov.sgc.powercontrol.verticles;

import io.vertx.core.AbstractVerticle;
import java.io.File;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.sql.Timestamp;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Julian Peña
 */
public class LoggerVerticle extends AbstractVerticle {

    public static final String ADDRESS = "LoggerVerticleAddress";

    private RandomAccessFile appLogFile = null;

    @Override
    public void start() throws Exception {

        super.start();

        appLogFile = new RandomAccessFile(new File("power-control.log"), "rw");

        vertx.eventBus().consumer(ADDRESS, logAddresshandler -> {
            String message = ((String) logAddresshandler.body()).trim();
            if (message.length() > 0) {
                log(message);
            }
        });
    }

    /**
     * Appends the given message to the global log file including a timestamp.
     *
     * @param message
     */
    private void log(String message) {
        try {
            Timestamp timestamp = new Timestamp(System.currentTimeMillis());
            appLogFile.seek(appLogFile.length());
            appLogFile.writeBytes(timestamp.toString() + "\t\t" + message + "\n");
        } catch (IOException ex) {
            Logger.getLogger(LoggerVerticle.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
