/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package co.gov.sgc.powercontrol.verticles;

import co.gov.sgc.powercontrol.pojos.Relay;
import co.gov.sgc.powercontrol.pojos.Station;
import com.google.gson.Gson;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpHeaders;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.StaticHandler;
import io.vertx.ext.web.templ.JadeTemplateEngine;
import java.util.HashMap;
import java.util.LinkedList;

/**
 *
 * @author ANDRESFELIPE
 */
public class Server extends AbstractVerticle {

    private EventBus eventbus;

    @Override
    public void start() {

        eventbus = vertx.eventBus();

        Router r = Router.router(vertx);
        JadeTemplateEngine jade = JadeTemplateEngine.create();
        r.route().handler(BodyHandler.create());
        r.route("/webroot/*").handler(StaticHandler.create("webroot"));
        
        r.get("/").handler(context -> {
            jade.render(context, "webroot/index.jade", respuesta -> {
                if (respuesta.succeeded()) {
                    context.response().end(respuesta.result());
                } else {
                    context.fail(respuesta.cause());
                }
            });
        });
        //enrutamiento para todo el crud de las estaciones
        r.get("/api/Estaciones").handler(this::getStation);
        r.post("/api/Estaciones").handler(this::add);
        r.get("/api/Estaciones/:idStation").handler(this::getRelays);
        r.put("/api/Estaciones/:id").handler(this::update);
        r.delete("/api/Estaciones/:id").handler(this::del);
        //enrutamiento para todo el crud de relays
        r.post("/api/Relays").handler(this::addRelay);
        r.put("/api/Relays/:id").handler(this::editRelay);

        // Serve the non private static pages
        r.route().handler(StaticHandler.create());
        // start a HTTP web server on port 8080
        vertx.createHttpServer().requestHandler(r::accept).listen(8080);
    }
    
    /**
     * agrega un relay
     *
     * @param r
     */
    public void addRelay(RoutingContext r) {
        Relay rele = Json.decodeValue(r.getBodyAsString(), Relay.class);
        System.out.println(rele.getStatus());
        System.out.println(rele.getStationId());
        r.response().setStatusCode(201)
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(Json.encodePrettily(""));
//        eventbus.send(DBClientVerticle.QUERY_ADD_RELAY, Json.encodePrettily(rele), Handler -> {
//            if (Handler.failed()) {
//                eventbus.send(LoggerVerticle.ADDRESS, "Fail add station");
//            } else {
//                eventbus.send(LoggerVerticle.ADDRESS, "Ok add station");
//            }
//        });
    }
    /**
     * edita un relay 
     * @param r 
     */
    public void editRelay(RoutingContext r) {
        Relay rele = Json.decodeValue(r.getBodyAsString(), Relay.class);
        double id = Double.parseDouble(r.request().getParam("id"));
        rele.setId(id);
        System.out.println(rele.getId());
        System.out.println(rele.getCode());
        r.response().setStatusCode(201)
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(Json.encodePrettily(""));
//        eventbus.send(DBClientVerticle.QUERY_EDIT_STATION, Json.encodePrettily(rele), Handler -> {
//            if (Handler.failed()) {
//                eventbus.send(LoggerVerticle.ADDRESS, "Fail add station");
//            } else {
//                eventbus.send(LoggerVerticle.ADDRESS, "Ok add station");
//            }
//        });
    }

    /**
     * elimina una estacion
     *
     * @param r
     */
    private void del(RoutingContext r) {
        String id = r.request().getParam("id");
        eventbus.send(DBClientVerticle.QUERY_DEL_STATION, Json.encodePrettily(id), Handler -> {
            if (Handler.failed()) {
                eventbus.send(LoggerVerticle.ADDRESS, "Fail edit station");
            } else {
                eventbus.send(LoggerVerticle.ADDRESS, "Ok edit station");
            }
        });
        r.response().setStatusCode(201)
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(Json.encodePrettily(""));
    }
    
    public void getRelays(RoutingContext r){
        Relay rel = new Relay(1, "Rele1", "S0", true, 0);
        LinkedList<Relay> listR = new LinkedList<>();
        listR.add(rel);
        r.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(Json.encodePrettily(listR));
//        String idStation = r.request().getParam("idStation");
//        eventbus.send(DBClientVerticle.QUERY_ADDRESS, DBClientVerticle.QUERY_STATION_RELAYS + "," + idStation, Handler -> {
//            if (Handler.failed()) {
//                eventbus.send(LoggerVerticle.ADDRESS, "Fail get relay");
//            } else {
//                Relay[] relays = Json.decodeValue((String) Handler.result().body(),Relay[].class);
//                r.response()
//                        .putHeader("content-type", "application/json; charset=utf-8")
//                        .end(Json.encodePrettily(relays));
//            }
//        });
    }

    /**
     * agrega una estacion
     *
     * @param r
     */
    public void add(RoutingContext r) {
        Station station = Json.decodeValue(r.getBodyAsString(), Station.class);
        r.response().setStatusCode(201)
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(Json.encodePrettily(""));
        eventbus.send(DBClientVerticle.QUERY_ADD_STATION, Json.encodePrettily(station), Handler -> {
            if (Handler.failed()) {
                eventbus.send(LoggerVerticle.ADDRESS, "Fail add station");
            } else {
                eventbus.send(LoggerVerticle.ADDRESS, "Ok add station");
            }
        });
    }

    /**
     * edita una estacion
     *
     * @param r
     */
    public void update(RoutingContext r) {
        Station station = Json.decodeValue(r.getBodyAsString(), Station.class);
        double id = Double.parseDouble(r.request().getParam("id"));
        station.setId(id);
        r.response().setStatusCode(201)
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(Json.encodePrettily(""));
        eventbus.send(DBClientVerticle.QUERY_EDIT_STATION, Json.encodePrettily(station), Handler -> {
            if (Handler.failed()) {
                eventbus.send(LoggerVerticle.ADDRESS, "Fail add station");
            } else {
                eventbus.send(LoggerVerticle.ADDRESS, "Ok add station");
            }
        });
    }

    /**
     * obtiene todas las estaciones de la BD
     *
     * @param r
     */
    public void getStation(RoutingContext r) {
        Station s=new Station(0, "name", "127.0.0.0", 1122);
        LinkedList<Station> estaciones=new LinkedList<>();
        estaciones.add(s);
        r.response()
                .putHeader("content-type", "application/json; charset=utf-8")
                .end(Json.encodePrettily(estaciones));
//        eventbus.send(DBClientVerticle.QUERY_ADDRESS, DBClientVerticle.QUERY_ALL_STATIONS, Handler -> {
//            if (Handler.failed()) {
//                eventbus.send(LoggerVerticle.ADDRESS, "Fail get all stations");
//            } else {
//                Station[] updatedStations = Json.decodeValue((String) Handler.result().body(), Station[].class);
//                r.response()
//                        .putHeader("content-type", "application/json; charset=utf-8")
//                        .end(Json.encodePrettily(updatedStations));
//            }
//        });
    }

}
