package co.gov.sgc.powercontrol.verticles;

import co.gov.sgc.powercontrol.pojos.Station;
import co.gov.sgc.powercontrol.util.ResponseCode;
import com.google.gson.Gson;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.eventbus.EventBus;
import java.io.FileInputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

/**
 *
 * @author Julian Peña
 */
public class HeartbeatVerticle extends AbstractVerticle {

    public static final String ADDRESS = "HeartbeatAddress";

    //Reference to the verts event bus
    private EventBus eventBus;

    //Json parses
    private static final Gson GSON = new Gson();

    //Stations map
    private static final HashMap<Station, Long> STATIONS = new HashMap<>();

    //Magic word which prevents the station from being rebooted
    private String magicWord = "DT24254";

    //Hearbeat period defaults to 8 hours
    private int hearbeatPeriod = 8 * 60 * 60;

    @Override
    public void start() throws Exception {

        super.start();

        eventBus = vertx.eventBus();

        eventBus.send(LoggerVerticle.ADDRESS, "Reading configuration ...");

        Properties properties = new Properties();
        properties.load(new FileInputStream("config.properties"));
        magicWord = properties.getProperty("magicWord", magicWord);
        hearbeatPeriod = Integer.parseInt(properties.getProperty("hearbeatPeriod", hearbeatPeriod + "")) * 1000;
        eventBus.send(LoggerVerticle.ADDRESS, "Magic word: " + magicWord);
        eventBus.send(LoggerVerticle.ADDRESS, "Hearbeat period: " + hearbeatPeriod / 1000 + " seconds");

        eventBus.send(LoggerVerticle.ADDRESS, "Starting Heartbeat service ...");
        eventBus.send(LoggerVerticle.ADDRESS, "Querying stations ...");
        loadStations();

        eventBus.consumer(ADDRESS, reloadStationsHandler -> {
            eventBus.send(LoggerVerticle.ADDRESS, "Reloading stations from database ...");
            loadStations();
        });
    }

    /**
     * Loads in memory the current stations in database, cancels the old
     * heartbeat timers and sets the new ones.
     */
    private void loadStations() {
        eventBus.send(DBClientVerticle.QUERY_ADDRESS, DBClientVerticle.QUERY_ALL_STATIONS, replyHandler -> {
            if (replyHandler.failed()) {
                eventBus.send(LoggerVerticle.ADDRESS, "Station list not updated");
            } else {

                //Cancel current hearbeat timers and remove station from map
                STATIONS.entrySet().stream().forEach(entry -> {
                    vertx.cancelTimer(STATIONS.remove(entry.getKey()));
                });

                Station[] updatedStations = GSON.fromJson(((String) replyHandler.result().body()), Station[].class);

                //regenerate stations map and hearbeat timers
                for (Station station : updatedStations) {
                    STATIONS.put(station, vertx.setPeriodic(hearbeatPeriod, hearbeatHandler -> {
                        sendHearbeat(station);
                    }));
                    eventBus.send(LoggerVerticle.ADDRESS, "Adding station: " + station);
                }

                //If the hearbeat period higher than 1 minute then force a hearbeat inmediately
                //for all stations
                if (hearbeatPeriod > 60000) {
                    STATIONS.entrySet().stream().map(Map.Entry::getKey).forEach(station -> {
                        sendHearbeat(station);
                    });
                }
            }
        });
    }

    /**
     * Send the magicWord String to the given IP and port via TCP.
     *
     * @param ip
     * @param port
     */
    private void sendHearbeat(Station station) {
        eventBus.send(LoggerVerticle.ADDRESS, "Sending heartbeat to station " + station.getName() + " at " + station.getIp() + ":" + station.getPort());
        eventBus.send(TCPClientVerticle.ADDRESS, station.getIp() + "," + station.getPort() + "," + magicWord + "\r\n", replyHandler -> {
            if (replyHandler.failed()) {
                eventBus.send(LoggerVerticle.ADDRESS, "No Hearbeat sent to station at " + station.getName());
            } else {
                String response = ((String) replyHandler.result().body()).trim();
                try {
                    switch (ResponseCode.valueOf(response)) {
                        case OK:
                            vertx.eventBus().send(LoggerVerticle.ADDRESS, "Station " + station.getName() + " " + ResponseCode.OK.toString());
                            break;
                        case TIMEOUT:
                            vertx.eventBus().send(LoggerVerticle.ADDRESS, "Station " + station.getName() + " " + ResponseCode.TIMEOUT.toString());
                            break;
                    }
                } catch (IllegalArgumentException e) {
                    vertx.eventBus().send(LoggerVerticle.ADDRESS, "Station " + station + " is sending garbage: " + response);
                }
            }
        });
    }

}
