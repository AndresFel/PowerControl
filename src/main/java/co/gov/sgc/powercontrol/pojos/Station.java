package co.gov.sgc.powercontrol.pojos;

import java.util.ArrayList;

/**
 *
 * @author Julian Peña
 */
public class Station {
    
    private double id = 0;
    private String name = "default", ip = "127.0.0.1";
    private int port = 8000;
    
    private ArrayList<Relay> relays = new ArrayList<>();

    public Station(double id, String name, String ip, int port) {
        this.id = id;
        this.name = name;
        this.ip = ip;
        this.port = port;
    }

    public Station() {
    }
    

    /**
     * 
     * @return the station id
     */
    public double getId() {
        return id;
    }
    /**
     * set the station id
     * @param id 
     */
    public void setId(double id) {
        this.id = id;
    }
    

    /**
     * 
     * @return the station name
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the station name
     * @param name 
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * 
     * @return the station ip
     */
    public String getIp() {
        return ip;
    }

    /**
     * Sets the station ip
     * @param ip 
     */
    public void setIp(String ip) {
        this.ip = ip;
    }

    /**
     * 
     * @return The stations TCP port
     */
    public int getPort() {
        return port;
    }

    /**
     * Sets the station TCP port
     * @param port 
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * 
     * @return an ArrayList of the station available relays
     */
    public ArrayList<Relay> getRelays() {
        return relays;
    }
    
    public void addRelay(Relay relay){
        this.relays.add(relay);
    }

    /**
     * Sets the ArrayList of relays for this station
     * @param relays 
     */
    public void setRelays(ArrayList<Relay> relays) {
        this.relays = relays;
    }

    @Override
    public String toString() {
        return this.name + "@" + this.ip + ":" + this.port;
    }
    
    
    
}
