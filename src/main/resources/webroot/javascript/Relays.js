/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
var station;

$(function () {
    iniciarDialogRelay();
    
});
/*
 * agrega un rele
 * envia el objeto en json
 * @param {type} name
 * @param {type} code
 * @param {type} status
 * @returns {undefined}
 */
function crearRelay(name,code,status) {
    $.post("/api/Relays/", JSON.stringify({name: name, code: code, status: status, stationId:station}), function () {
        loadRelays();
    }, "json");
}
/*
 * edita un rele
 * @param {type} name
 * @param {type} code
 * @param {type} status
 * @returns {undefined}
 */
function editarRelay(id, name, code, status) {
    alert(id + " "+ name + " "+ code + " " + status);
    $.ajax({
        method: "PUT",
        url: "/api/Relays/" + id,
        data: JSON.stringify({name: name, code: code, status: status})
    }).done(function () {
        load();
    });
}
/*
 * crea el metod para el tercer boton de la tabla estaciones 
 * llama el metodo loadRelays
 * @returns {undefined}
 */
function listarRelays() {
    $(".listar-relays").unbind().click(function () {
        var idStation = $(this).data("id");
        loadRelays(idStation);
        station = idStation;
        botonesRelays();
    });
}
/*
 * hace la peticion get
 * los objetos los recibe en un json 
 * carga en la tabla los relays
 * @param {type} idStation
 * @returns {undefined}
 */
function loadRelays(idStation) {
    $("#thead").children().remove();
    $("<tr><td> Name </td><td> Code </td><td> Status </td></tr>").appendTo("#thead");
    var i;
    var estado;
    $("#body").children().remove();
    $.getJSON("/api/Estaciones/" + idStation, function (data) {
        for (i = 0; i < data.length; i++) {
            estado = Estado(data[i].status);
            $("<tr><td>" + data[i].name + "</td><td>" + data[i].code + "<td>" + estado + "</td>" + "<td>" +
                    "<button class='btn btn-danger btn-sm onOff-relay' data-id='" + data[i].id + "'>" +
                    "   <span class='glyphicon glyphicon-off'></span>" +
                    "</button>" +
                    "&nbsp;" +
                    "<button data-action='edit' class='btn btn-primary btn-sm editar-relay' " +
                        "data-toggle='modal' " +
                        "data-target='#relaymodal' " +
                        "data-id='" + data[i].id + "' " +
                        "data-nombre='" + data[i].name + "' " +
                        "data-code='" + data[i].code + "' " +
                        "data-status='" + data[i].status + "'>" +
                        "<span class='glyphicon glyphicon-pencil'></span>" +
                    "</button>" +
                    "&nbsp;" +
                    "<button class='btn btn-danger btn-sm eliminar-relay' data-id='" + data[i].id + "'>" +
                    "   <span class='glyphicon glyphicon-minus'></span>" +
                    "</button>" +
                    "</tr>").appendTo("#body");
            //$("#relays").append('<option value="'+data[i].id+'">'+data[i].name+'</option>');
        }
        OnOff();
    });
}
/*
 * cambia el bool del relay a on/off
 * @param {type} status
 * @returns {String} on?off
 */
function Estado(status){
    if (status === true) {
        return "On";
    }else{
        return "Off";
    }
}
/*
 * metodo para encender/apagar el relay
 * @returns {undefined}
 */
function OnOff(){
    $(".onOff-relay").unbind().click(function () {
        var idRele = $(this).data("id");
        onOffRele(idRele);
    });
}

function onOffRele(idRele){
    alert("funciona"+idRele);
}

/*
 * botones para los relays
 * pone el titulo en el div
 */
function botonesRelays(){
    $("#botones").children().remove();
    $("<h1> Relays </h1>" +
            //"<select id='relays' class='col-sm-4'>  </select>"+
            "<button class='pull-right btn btn-primary volver' data-action='volver'>"+
            "<span class='glyphicon glyphicon-circle-arrow-left'></span>"+
            "<button class='pull-right btn btn-primary add-relay' data-action='add' data-toggle='modal', data-target='#relaymodal'>"+
            "<span class='glyphicon glyphicon-plus'></span>").appendTo("#botones");
    Regresar();
}
/*
 * 
 * volver a cargar las estaciones
 */
function Regresar() {
    $(".volver").unbind().click(function () {
        load();
    });
}

function iniciarDialogRelay() {
    $("#relaymodal").on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var action = button.data('action');
        var id = button.data('id');
        var btnGuardar = $("#btn-guardarRelay");
        btnGuardar.unbind();
        var modal = $(this);
        if (action === "add") {
            modal.find('.modal-title').text("Agregar Relay");
            modal.find('#nombre-relay').val("");
            modal.find('#code-relay').val("");
            modal.find('#status-relay').val("");
            btnGuardar.click(function () {
                    crearRelay($("#nombre-relay").val(), $("#code-relay").val(), $("#status-relay").val());
                    $('#relaymodal').modal('toggle');
            });
        } else {
            modal.find('.modal-title').text("Editar Relay");
            modal.find('#nombre-relay').val(button.data("nombre"));
//            modal.find('#code-relay').text(button.data("code"));
//            modal.find('#status-relay').text(button.data("status"));
            btnGuardar.click(function () {
                    editarRelay(id, $("#nombre-relay").val(), $("#code-relay").val(), $("#status-relay").val());
                    $('#relaymodal').modal('toggle');
            });
        }
    });
}






