/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(function () {
    load();
    iniciarDialog();
});
/*
 * crea una estacion
 * los datos los manda en un objeto json
 * @param {type} nombre
 * @param {type} ip
 * @param {type} puerto
 * @returns {undefined}
 */
function createEstacion(nombre, ip, puerto) {
    $.post("/api/Estaciones/", JSON.stringify({name: nombre, ip: ip, port: puerto}), function () {
        load();
        alert("load");
    }, "json");
}
/*
 * elimina una estacion
 * @param {type} id
 * @returns {undefined}
 */
function eliminarEstacion(id) {
    $.ajax({
        method: "DELETE",
        url: "/api/Estaciones/" + id
    }).done(function () {
        load();
    });
}
/*
 * hace peticion put
 * edita una estacions
 * los daots los envia en un json
 * @param {type} id
 * @param {type} name
 * @param {type} ip
 * @param {type} port
 * @returns {undefined}
 */
function editarEstacion(id, name, ip, port) {
    $.ajax({
        method: "PUT",
        url: "/api/Estaciones/" + id,
        data: JSON.stringify({name: name, ip: ip, port: port})
    }).done(function () {
        load();
    });
}

function eliminar() {
    $(".eliminar-estacion").unbind().click(function () {
        var id = $(this).data("id");
        eliminarEstacion(id);
    });
}
/*
 * hace la peticion get
 * los objetos los recibe en un json
 * carga en la tabla las estaciones
 * @returns {undefined}
 */
function load() {
    botonesEstaciones();
    $("#thead").children().remove();
    $("<tr><td> Name </td><td> Ip </td><td> Port </td></tr>").appendTo("#thead");
    var i;
    $("#body").children().remove();
    $.getJSON("/api/Estaciones", function (data) {
        for (i = 0; i < data.length; i++) {
            $("<tr><td>" + data[i].name + "</td><td>" + data[i].ip + "</td>" + "<td>" + data[i].port + "</td>" +
                    "<td>" +
                    "<button data-action='edit' class='btn btn-primary btn-sm editar-estacion' " +
                    "data-toggle='modal' " +
                    "data-target='#estacionmodal' " +
                    "data-id='" + data[i].id + "' " +
                    "data-nombre='" + data[i].name + "' " +
                    "data-ip='" + data[i].ip + "' " +
                    "data-puerto='" + data[i].port + "'>" +
                    "<span class='glyphicon glyphicon-pencil'></span>" +
                    "</button>" +
                    "&nbsp;" +
                    "<button class='btn btn-danger btn-sm eliminar-estacion' data-id='" + data[i].id + "'>" +
                    "   <span class='glyphicon glyphicon-minus'></span>" +
                    "</button>" +
                    "&nbsp;" +
                    "<button class='btn btn-info btn-sm listar-relays' data-id='" + data[i].id + "'>" +
                    "   <span class='glyphicon glyphicon-share-alt'></span>" +
                    "</button>" +
                    "</td>" +
                    "</tr>").appendTo("#body");
        }
        eliminar();
        listarRelays();
    });
}
/*
 * pone el titulo al div 
 * agregar booton add para las estaciones
 */
function botonesEstaciones(){
    $("#botones").children().remove();
    $("<h1> Estaciones </h1>").appendTo("#botones");
    $("<button class='pull-right btn btn-primary add-estacion' data-action='add' data-toggle='modal', data-target='#estacionmodal'>"+
            "<span class='glyphicon glyphicon-plus'></span>").appendTo("#botones");
}

/*
 * cargar el dialog de las estaciones para agregar y editar
 */
function iniciarDialog() {
    $("#estacionmodal").on('show.bs.modal', function (event) {
        var button = $(event.relatedTarget);
        var action = button.data('action');
        var id = button.data('id');
        var btnGuardar = $("#btn-guardar");
        btnGuardar.unbind();
        var modal = $(this);
        if (action === "add") {
            modal.find('.modal-title').text("Agregar Estacion");
            modal.find('#nombre-estacion').val("");
            modal.find('#ip-estacion').val("");
            modal.find('#puerto-estacion').val("");
            btnGuardar.click(function () {
                createEstacion($("#nombre-estacion").val(), $("#ip-estacion").val(), $("#puerto-estacion").val());
                $('#estacionmodal').modal('toggle');
            });
        } else {
            modal.find('.modal-title').text("Editar Estacion");
            modal.find('#nombre-estacion').val(button.data("nombre"));
            modal.find('#ip-estacion').val(button.data("ip"));
            modal.find('#puerto-estacion').val(button.data("puerto"));
            btnGuardar.click(function () {
                editarEstacion(id, $("#nombre-estacion").val(), $("#ip-estacion").val(), $("#puerto-estacion").val());
                $('#estacionmodal').modal('toggle');
            });
        }
    });
}
